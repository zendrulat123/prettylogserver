package main

import (
	"context"

	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func run(ctx context.Context, x string, y string) ([]string, string) {

	var strs []string
	newstrs := x
	for i := 0; i < 1; i++ {
		strs = append(strs, y)
	}

	passContext(ctx, strs, newstrs)

	return strs, newstrs
}

func run2(ctx context.Context, x string, y string) ([]string, string) {

	var strs []string
	newstrs := y
	for i := 0; i < 1; i++ {
		strs = append(strs, x)
	}

	passContext(ctx, strs, newstrs)

	return strs, newstrs
}
func init() {

	tpl = template.Must(template.ParseGlob("templates/*"))
}
func main() {
	c := context.Background()
	run(c, "another one", "stringssss")
	run2(c, "anodsfther one", "strifdsdsfdsfds")
	http.HandleFunc("/show", show)
	log.Fatal(http.ListenAndServe(":8080", nil))

}

func show(res http.ResponseWriter, req *http.Request) {

	err := tpl.ExecuteTemplate(res, "show.html", nil)
	if err != nil {
		log.Fatalln("template didn't execute: ", err)
	}
}
